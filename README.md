Initial source: https://github.com/chenhaiq/mt7610u_wifi_sta_v3002_dpo_20130916
- Mod for kernel 4.15
- Added  TP-Link Archer T2UH


Build steps:
$ make
$ sudo make install
$ cp RT2870STA.dat /etc/Wireless/RT2870STA/RT2870STA.dat
$ sudo systemctl restart NetworkManager.service

Enjoy!